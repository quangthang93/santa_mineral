<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container type2">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>お問い合わせ</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-contact.png" alt="">
          <span>お問い合わせ</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col is-passed">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">問い合わせ内容入力</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">内容確認</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">完了</p>
                <p></p>
              </div>
            </div>
            <p class="c-contact__message">下記の内容でよろしければ「送信する」ボタンをクリックしてください。 <br>項目を修正する場合は「修正する」ボタンから編集画面にお戻りください。</p>
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ区分</span>
                    </label>
                    <div class="c-form__row__field">
                      法人 <input type="hidden" name="fullname" value="法人">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">会社名</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容<input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">部署名</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お名前</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お名前</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ふりがな</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">メールアドレス</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">電話番号</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">郵便番号</span>
                    </label>
                    <div class="c-form__row__field">
                      0000000 <input type="hidden" name="fullname" value="0000000">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">都道府県</span>
                    </label>
                    <div class="c-form__row__field">
                      東京都 <input type="hidden" name="fullname" value="東京都">
                    </div>
                  </div>

                  <div class="c-form__row type2">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">住所</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row type2">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">建物名</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ内容</span>
                    </label>
                    <div class="c-form__row__field">
                      入力内容 <input type="hidden" name="fullname" value="入力内容">
                    </div>
                  </div>
                </div>
                <ul class="c-contact__action">
                  <li class="c-contact__action__button">
                    <input type="submit" name="submitConfirm" value="送信する" class="c-button action">
                    <span class="c-button--bg"></span>
                  </li>
                  <li class="c-contact__action__button">
                    <input type="submit" name="submitConfirm" value="修正する" class="c-button action">
                    <span class="c-button--bg type2"></span>
                  </li>
              </li>
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>