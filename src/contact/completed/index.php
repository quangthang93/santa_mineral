<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container type2">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>お問い合わせ</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-contact.png" alt="">
          <span>お問い合わせ</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col is-passed">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">問い合わせ内容入力</p>
                <p></p>
              </div>
              <div class="c-steps__col is-passed">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">内容確認</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">完了</p>
                <p></p>
              </div>
            </div>
            <p class="title-lv3-2 mgb-35 align-center">お問い合わせいただき、ありがとうございました。</p>
            <p class="desc2">お問い合わせ送信が完了いたしました。担当スタッフが内容を確認し、ご連絡させていただきます。しばらく経っても連絡がない場合は、恐れ入りますが、再度お問い合わせください。</p>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>