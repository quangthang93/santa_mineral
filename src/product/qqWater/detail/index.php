<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">
  <div class="p-end--cnt">
    <div class="container">
      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li><a href="/product">技術・製品情報</a></li>
            <li>QQウォーター</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->
      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-service.png" alt="">
          <span>技術・製品情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->
      <div class="p-service">
        <div class="p-service--ttl">
          <h2 class="section-title-ep">QQ Water</h2>
        </div>
        <div class="p-service--cnt">
          <div class="p-service--detail">
            <div class="p-service--detail-product col2">
              <div class="p-service--detail-product-infor col2-item">
                <h3 class="title-lv2">テラ・プロテクト CAC-717</h3>
                <p class="desc">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</p>
                <a href="/contact" class="link-pink"><span>シリーズのご注文はこちら</span></a>
              </div>
              <div class="p-service--detail-product-thumb col2-item">
                <img src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
              </div>
            </div><!-- ./p-service--detail-product -->
            <div class="p-service--detail-infor">
              <div class="p-service--detail-row">
                <h4 class="title-lv3">やさしく強い除菌水</h4>
                <ul class="p-service--features">
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">99.99％ウイルス除去、除菌</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features01.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">多くのウイルスや菌に対応し、FDAに登録されています。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">植物由来だから安心・安全に使える</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features02.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">成分は水とメゾミネラルのみのため、誰でも安心・安全に使えます。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">アレルギーなどの問題も解決</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features03.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">化学薬品不使用で、アレルギーや薬剤耐性を起こしません。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">化学薬品不使用で環境にもやさしい</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features04.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">環境負荷をかけない除菌水として、SDGsに貢献します。</p>
                    </div>
                  </li>
                </ul><!-- ./p-service--features -->
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">ご利用のポイント</h4>
                <ul class="p-service--points">
                  <li class="p-service--points-item">
                    <div class="p-service--points-numberWrap">
                      <span class="p-service--points-label stroke-white">POINT</span>
                      <span class="p-service--points-number stroke-pink">1</span>
                    </div>
                    <div class="p-service--points-cnt">
                      <div class="p-service--points-cnt-inner">
                        <p class="p-service--points-cnt-ttl">手に触れるものをスプレーで簡単除菌</p>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--points-item">
                    <div class="p-service--points-numberWrap">
                      <span class="p-service--points-label stroke-white">POINT</span>
                      <span class="p-service--points-number stroke-pink">2</span>
                    </div>
                    <div class="p-service--points-cnt">
                      <div class="p-service--points-cnt-inner">
                        <p class="p-service--points-cnt-ttl">加湿器に入れて、空間除菌</p>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--points-item">
                    <div class="p-service--points-numberWrap">
                      <span class="p-service--points-label stroke-white">POINT</span>
                      <span class="p-service--points-number stroke-pink">3</span>
                    </div>
                    <div class="p-service--points-cnt">
                      <div class="p-service--points-cnt-inner">
                        <p class="p-service--points-cnt-ttl">化学薬品が使えない場所も安心除菌</p>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--points-item">
                    <div class="p-service--points-numberWrap">
                      <span class="p-service--points-label stroke-white">POINT</span>
                      <span class="p-service--points-number stroke-pink">4</span>
                    </div>
                    <div class="p-service--points-cnt">
                      <div class="p-service--points-cnt-inner">
                        <p class="p-service--points-cnt-ttl">壁やマスクにコーティングし持続除菌</p>
                      </div>
                    </div>
                  </li>
                </ul><!-- ./p-service--points -->
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">テラ・プロテクトのしくみ</h4>
                <div class="col2-75">
                  <div class="col2-75--left">
                    <p class="desc2">テラ・プロテクトに含まれる、特殊な電気を持つメゾミネラルが菌やウイルスを電気のチカラで壊します。メゾミネラルが持つ電気は植物から取り出した自然界のものなので、人体に悪い影響を与えずに除菌することができます。</p>
                  </div>
                  <div class="col2-75--right">
                    <img src="<?php echo $PATH;?>/assets/images/end/service/product02.png" alt="">
                  </div>
                </div>
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">効果・実証実験</h4>
                <p class="desc2 mgb-30">テラ・プロテクトはウイルスや菌を破壊し、除菌できることが証明されています。マウスノロウイルスを使った実験では、きれいな丸い形をしたウイルスが、テラ・プロテクトの使用により壊れていることがわかります。こうなったウイルスは感染力や毒性を失います。</p>
                <div class="thumb-wrapper">
                  <h3 class="title-lv4 mgb-15">テラ・プロテクト使用前</h3>
                  <p class="desc2 mgb-10">このテキストはキャプションです。このテキストはキャプションです。</p>
                  <div class="thumb align-center">
                    <img src="<?php echo $PATH;?>/assets/images/end/service/dummy.png" alt="">
                  </div>
                  <p class="desc2 mgt-10">ウイルスはきれいな丸い形をしています。</p>
                </div>
                <div class="col2">
                  <div class="col2-item">
                    <h3 class="title-lv4 mgb-15">テラ・プロテクト使用前</h3>
                    <div class="col2-item--thumb">
                      <img src="<?php echo $PATH;?>/assets/images/end/service/dummy.png" alt="">
                    </div>
                    <p class="desc2 mgt-10">ウイルスはきれいな丸い形をしています。</p>
                  </div>
                  <div class="col2-item">
                    <h3 class="title-lv4 mgb-15">テラ・プロテクト使用前</h3>
                    <div class="col2-item--thumb">
                      <img src="<?php echo $PATH;?>/assets/images/end/service/dummy.png" alt="">
                    </div>
                    <p class="desc2 mgt-10">ウイルスはきれいな丸い形をしています。</p>
                  </div>
                </div>
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">実証試験結果</h4>
                <p class="desc2 mgb-20">テラ・プロテクトは、多くのウイルスや菌への効果と安全性が証明されています。</p>
                <div class="table custom-width01">
                  <table>
                    <thead>
                      <tr>
                        <th>試験内容(報告日)</th>
                        <th>評価</th>
                        <th>試験結果</th>
                        <th>検査機関</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div><!-- ./p-service--detail-row -->
            </div><!-- ./p-service--detail-infor   -->
          </div><!-- ./p-service--detail -->
          <div class="p-service--infor">
            <div class="p-service--infor-row">
              <h3 class="title-lv2">技術情報</h3>
              <p class="desc2">QQウォーターに関する詳しい技術情報は、一般社団法人ミネラル活性化技術研究所でご案内しております。</p>
              <a href="" class="link-icon blank">一般社団法人ミネラル活性化技術研究所</a>
            </div><!-- ./p-service--infor-row -->
            <div class="p-service--infor-row">
              <h3 class="title-lv2">QQウォーターシリーズのご注文・お問い合わせ</h3>
              <p class="desc2 mgb-20">QQウォーターに関する詳しい技術情報は、一般社団法人ミネラル活性化技術研究所でご案内しております。</p>
              <h4 class="title-lv3">QQTechnology 総合窓口(オリックス株式会社)</h4>
              <p class="desc2 mgb-40">TEL 050-5491-2547 <br>受付時間 10:00-17:00 (土・日・祝・年末年始を除く)</p>
              <a href="/contact" class="link-pink"><span>ご注文専用お問い合わせフォーム</span></a>
            </div><!-- ./p-service--infor-row -->
            <div class="p-service--infor-row">
              <h3 class="title-lv2">技術に関するお問い合わせ</h3>
              <p class="desc2 mgb-40">当社技術に関するお問い合わせを受け付けております。</p>
              <a href="" class="link-border"><span>お問い合わせ(総合)</span></a>
            </div><!-- ./p-service--infor-row -->
          </div><!-- ./p-service--infor -->
        </div><!-- ./p-service--cnt -->
      </div><!-- ./p-service -->
    </div>
  </div>
  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">技術・製品情報に戻る</a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>