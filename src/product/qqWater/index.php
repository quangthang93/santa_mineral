<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li><a href="/product">技術・製品情報</a></li>
            <li>QQウォーター</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-service.png" alt="">
          <span>技術・製品情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-service">
        <div class="p-service--ttl">
          <h2 class="section-title-ep">QQ Water</h2>
        </div>
        <div class="p-service--intro">
          <div class="p-service--intro-inner">
            <div class="p-service--intro-cnt">
              <h3 class="title-lv2">QQウォーターについて</h3>
              <div class="p-service--intro-infor">
                <h4 class="title-lv3">自然由来のミネラルのチカラで生活環境をより快適に</h4>
                <p class="desc4">シリーズ全体の説明テキストが入ります。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
              </div>
            </div>
            <a href="/contact" class="link-pink"><span>シリーズのご注文はこちら</span></a>
          </div>
          <div class="p-service--intro-thumb">
            <img src="<?php echo $PATH;?>/assets/images/end/service/qqwater01.png" alt="">
          </div>
        </div><!-- ./p-service--intro -->
        <div class="p-company--infor">
          <ul class="anchor--list">
            <li>
              <a href="#subject01" class="link-anchor">製品ラインナップ</a>
            </li>
            <li>
              <a href="#subject02" class="link-anchor">技術情報</a>
            </li>
          </ul><!-- .p-company--infor-bar -->
          <div class="p-company--infor-cnt">
            <div class="p-faq--row" id="subject01">
              <h3 class="title-lv2">製品ラインナップ</h3>
              <div class="p-service--products">
                <ul class="p-service--products-list">
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                  <li class="p-service--products-item">
                    <h3 class="title-lv3">テラプロテクト</h3>
                    <div class="p-service--products-item-inner">
                      <div class="p-service--products-item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/product01.png" alt="">
                      </div>
                      <div class="p-service--products-item-cnt">
                        <h4 class="title-lv4">植物由来のミネラルのチカラでウイルスを99.99%除去！安心・安全な除菌水</h4>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div><!-- .p-faq--row -->   
            <div class="p-faq--row" id="subject02">
              <div class="p-service--infor">
                <div class="p-service--infor-row">
                  <h3 class="title-lv2">技術情報</h3>
                  <p class="desc2">QQウォーターに関する詳しい技術情報は、一般社団法人ミネラル活性化技術研究所でご案内しております。</p>
                  <a href="" class="link-icon blank">一般社団法人ミネラル活性化技術研究所</a>
                </div><!-- ./p-service--infor-row -->

                <div class="p-service--infor-row">
                  <h3 class="title-lv2">QQウォーターシリーズのご注文・お問い合わせ</h3>
                  <p class="desc2 mgb-20">QQウォーターに関する詳しい技術情報は、一般社団法人ミネラル活性化技術研究所でご案内しております。</p>
                  <h4 class="title-lv3">QQTechnology 総合窓口(オリックス株式会社)</h4>
                  <p class="desc2 mgb-40">TEL 050-5491-2547 <br>受付時間 10:00-17:00 (土・日・祝・年末年始を除く)</p>
                  <a href="/contact" class="link-pink"><span>ご注文専用お問い合わせフォーム</span></a>
                </div><!-- ./p-service--infor-row -->

                <div class="p-service--infor-row">
                  <h3 class="title-lv2">技術に関するお問い合わせ</h3>
                  <p class="desc2 mgb-40">当社技術に関するお問い合わせを受け付けております。</p>
                  <a href="" class="link-border"><span>お問い合わせ(総合)</span></a>
                </div><!-- ./p-service--infor-row -->
              </div><!-- ./p-service--infor -->
            </div><!-- .p-faq--row -->    
          </div><!-- .p-company--infor-cnt -->
        </div><!-- .p-company--infor -->
      </div><!-- ./p-service -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">技術・製品情報に戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>