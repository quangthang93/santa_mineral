<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">
  <div class="p-end--cnt">
    <div class="container">
      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li><a href="/product">技術・製品情報</a></li>
            <li>QQウォーター</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->
      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-service.png" alt="">
          <span>技術・製品情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->
      <div class="p-service">
        <div class="p-service--ttl">
          <h2 class="section-title-ep">QQ <span class="jp">栽培</span></h2>
        </div>
        <div class="p-service--intro">
          <div class="p-service--intro-inner">
            <div class="p-service--intro-cnt type2">
              <h3 class="title-lv2">QQ栽培について</h3>
              <div class="p-service--intro-infor">
                <h4 class="title-lv3">メゾミネラルで自然のチカラを蘇らせる真の無農薬栽培</h4>
                <p class="desc4">QQ栽培はメゾミネラルを応用した農業資材で実現する完全無農薬栽培です。<br>
                  どんな作物でも無農薬栽培を可能にし、作物の質を向上したり、収穫量を増加したりすることも可能です。<br>QQ栽培は作物が育つ土壌環境も改善するので、一度農薬に依存してしまった土壌でも、病気や害虫に強い作物が育つ豊かな土壌に蘇らせることができます。16</p>
              </div>
            </div>
          </div>
          <div class="p-service--intro-thumb">
            <img src="<?php echo $PATH;?>/assets/images/end/service/qqwater02.png" alt="">
          </div>
        </div><!-- ./p-service--intro -->
        <div class="p-service--cnt">
          <div class="p-service--detail">
            <div class="p-service--detail-infor">
              <div class="p-service--detail-row">
                <h4 class="title-lv3">QQ栽培の特長</h4>
                <ul class="p-service--features">
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">作物のチカラを引き出す真の無農薬栽培</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features05.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">無農薬栽培の実現だけでなく、作物のチカラを引き出し収穫量や質も向上します。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">作る人にも食べる人にもやさしい農業資材</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features06.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">自然由来の資材で、作る人から食べる人まですべての人の安心を守ります。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">砂漠などの農業が難しい土地での栽培を実現</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features07.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">農業が難しい土地でも、作物に必要な水や酸素、栄養素を蓄える環境に整えます。</p>
                    </div>
                  </li>
                  <li class="p-service--features-item">
                    <div class="p-service--features-ttlWrap">
                      <h5 class="p-service--features-item-ttl">化学薬品不使用で環境にもやさしい</h5>
                      <div class="p-service--features-item-thumb">
                        <img src="<?php echo $PATH;?>/assets/images/end/service/icon-features04.svg" alt="">
                      </div>
                    </div>
                    <div class="p-service--features-cnt">
                      <p class="p-service--features-cnt-ttl">環境負荷をかけない除菌水として、SDGsに貢献します。</p>
                    </div>
                  </li>
                </ul><!-- ./p-service--features -->
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">従来の栽培方法とQQ栽培の違い</h4>
                <div class="mgb-40">
                  <h5 class="title-lv4 mgb-20">従来の栽培方法</h5>
                  <p class="desc2">農薬に依存した栽培では、年月が経つごとに土壌が弱ってしまいます。その結果、農薬や化学肥料の量が年々増えていき、環境負荷やコストがどんどん増えてしまいます。</p>
                </div>
                <div class="mgb-40">
                  <h5 class="title-lv4 mgb-20">QQ栽培</h5>
                  <p class="desc2">QQ栽培の場合、土壌が本来持つべき自然のチカラを蘇らせるため、資材の使用量を年々減らしながらも、豊かな作物を育てる環境を維持することができます。</p>
                </div>
                <div class="mgt-60 align-center">
                  <img src="<?php echo $PATH;?>/assets/images/end/service/qqwater03.png" alt="">
                </div>
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">実用例</h4>
                <div class="p-service--products">
                  <ul class="p-service--products-list mgb-80">
                    <li class="p-service--products-item">
                      <div class="p-service--products-item-inner">
                        <div class="p-service--products-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/sample01.png" alt="">
                        </div>
                        <div class="p-service--products-item-cnt type2">
                          <div class="p-service--products-item-infor">
                            <h4 class="title-lv4">いちご農家での無農薬栽培(茨城県)</h4>
                            <p class="desc2">難しいと言われているいちごの無農薬栽培を実現しました。実がつく時期がのびて、収穫量も増加し、育ったいちごは人にうれしい抗酸化作用も高まっていました。</p>
                          </div>
                          <div class="link-borderWrap">
                            <a href="" class="link-border"><span>詳しくみる</span></a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-service--products-item">
                      <div class="p-service--products-item-inner">
                        <div class="p-service--products-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/sample02.png" alt="">
                        </div>
                        <div class="p-service--products-item-cnt type2">
                          <div class="p-service--products-item-infor">
                            <h4 class="title-lv4">トマト農家での無農薬栽培(福岡県)</h4>
                            <p class="desc2">農薬や連作による土壌疲労や不作などの問題を解決するため、ＱＱ栽培を導入しました。トマトの苗は健康的になり、多くの実がなりました。</p>
                          </div>
                          <div class="link-borderWrap">
                            <a href="" class="link-border"><span>詳しくみる</span></a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-service--products-item">
                      <div class="p-service--products-item-inner">
                        <div class="p-service--products-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/sample03.png" alt="">
                        </div>
                        <div class="p-service--products-item-cnt type2">
                          <div class="p-service--products-item-infor">
                            <h4 class="title-lv4">バラ農園での無農薬栽培(熊本県)</h4>
                            <p class="desc2">無農薬栽培が不可能と言われているバラでも、農薬を使わない栽培に成功しています。葉の艶や花の色も鮮やかになり美しいバラが育ちました。</p>
                          </div>
                          <div class="link-borderWrap">
                            <a href="" class="link-border"><span>詳しくみる</span></a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-service--products-item">
                      <div class="p-service--products-item-inner">
                        <div class="p-service--products-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/sample01.png" alt="">
                        </div>
                        <div class="p-service--products-item-cnt type2">
                          <div class="p-service--products-item-infor">
                            <h4 class="title-lv4">松林での松くい虫対策(島根県)</h4>
                            <p class="desc2">対策が難しかった松枯れ病の原因、松くい虫の駆除を実現するため、QQ栽培を実施しました。松の木はみるみる元気になり、これまで見られなかった新芽がたくさん生えてきました。</p>
                          </div>
                          <div class="link-borderWrap">
                            <a href="" class="link-border"><span>詳しくみる</span></a>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-service--products-item">
                      <div class="p-service--products-item-inner">
                        <div class="p-service--products-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/end/service/sample02.png" alt="">
                        </div>
                        <div class="p-service--products-item-cnt type2">
                          <div class="p-service--products-item-infor">
                            <h4 class="title-lv4">砂漠でのレタス栽培(ゴビ砂漠)</h4>
                            <p class="desc2">QQ栽培の実施により、作物に必要な水分、酸素、栄養分を砂漠の土地にも蓄えることができました。その結果レタスの栽培にも成功しています。</p>
                          </div>
                          <div class="link-borderWrap">
                            <a href="" class="link-border"><span>詳しくみる</span></a>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div><!-- ./p-service--detail-row -->
              <div class="p-service--detail-row">
                <h4 class="title-lv3">実証試験結果</h4>
                <p class="desc2 mgb-20">QQ栽培の資材は、人にやさしい安全性が実験によって証明されています。</p>
                <div class="table custom-width01">
                  <table>
                    <thead>
                      <tr>
                        <th>試験内容(報告日)</th>
                        <th>評価</th>
                        <th>試験結果</th>
                        <th>検査機関</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                      <tr>
                        <td data-label="試験内容(報告日)"><a href="" class="link-icon pdf">試験結果PDFを何点か掲載(選定中)</a>
                          <p class="desc2">(20xx年xx月xx日)</p>
                        </td>
                        <td data-label="評価">○</td>
                        <td data-label="試験結果">このテキストはダミーです。 <br>このテキストはダミーです。</td>
                        <td data-label="検査機関">一般財団法人北里環境化学センター</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div><!-- ./p-service--detail-row -->
            </div><!-- ./p-service--detail-infor   -->
          </div><!-- ./p-service--detail -->
          <div class="p-service--infor">
            <div class="p-service--infor-row">
              <h3 class="title-lv2">技術情報</h3>
              <p class="desc2">QQウォーターに関する詳しい技術情報は、一般社団法人ミネラル活性化技術研究所でご案内しております。</p>
              <a href="" class="link-icon blank">一般社団法人ミネラル活性化技術研究所</a>
            </div><!-- ./p-service--infor-row -->
            <div class="p-service--infor-row">
              <h3 class="title-lv2">QQ栽培に関するご相談・お問い合わせ</h3>
              <p class="desc2 mgb-40">当社技術に関するお問い合わせを受け付けております。</p>
              <a href="" class="link-border"><span>お問い合わせ(総合)</span></a>
            </div><!-- ./p-service--infor-row -->
          </div><!-- ./p-service--infor -->
        </div><!-- ./p-service--cnt -->
      </div><!-- ./p-service -->
    </div>
  </div>
  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">技術・製品情報に戻る</a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>