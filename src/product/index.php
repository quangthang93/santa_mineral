<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">
  <div class="p-end--cnt">
    <div class="container">
      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>技術・製品情報</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->
      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-service.png" alt="">
          <span>技術・製品情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->
      <div class="p-service type2">
        <section class="p-top--technology">
          <div class="p-top--technology-cnt">
            <div class="p-top--technology-cnt-infor">
              <div class="title-boxWrap">
                <div class="ani-border type2">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
                <h3 class="title-box type2">QQ TECHNOLOGYとは</h3>
              </div>
              <p class="desc">QQテクノロジーは、自然由来のミネラルを抽出、水と融合させることで、特定の電磁波を発生させる物質を作り出す全く新しい技術です。化学物質の残留性が一切ない特徴を活かし、医療、生活環境、農業、工業など、多岐の分野でその機能性を発揮します。</p>
               <p class="desc">この技術は、対象物に対し有効な働きをもたらし、病気の根本治療や自然環境の再生を可能にする画期的な技術開発につながることで、世界的に注目を集めています。</p>
            </div>
            <div class="p-top--technology-cnt-img">
              <img src="<?php echo $PATH;?>/assets/images/common/icon-bear.svg" alt="">
            </div>
          </div><!-- ./p-top--technology-cnt -->
          <div class="p-top--technology-direct">
            <a class="link-pink faderight" href=""><span>QQ TECHNOLOGYとは</span></a>
          </div><!-- ./p-top--technology-direct -->
        </section><!-- ./p-top--technology -->
        <section class="p-top--service">
          <div class="p-top--service-row">
            <p class="p-top--service-row-label">
              <img class="pc-only" src="<?php echo $PATH;?>/assets/images/common/title-product.svg" alt="">
              <img class="sp-only" src="<?php echo $PATH;?>/assets/images/common/title-product-sp.png" alt="">
            </p>
            <div class="p-top--service-list">
              <div class="p-top--service-item blue">
                <div class="title-boxWrap">
                  <div class="ani-border type2">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                  <h3 class="title-box type2">QQの技術を応用した製品</h3>
                </div>
                <a href="" class="link">
                  <div class="p-top--service-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/product01.png" alt="">
                  </div>
                  <div class="p-top--service-item-infor">
                    <div class="p-top--service-item-inner">
                      <p class="p-top--service-item-des">世界初の植物テラヘルツ波機能水</p>
                      <h4 class="p-top--service-item-ttl">QQ Water</h4>
                    </div>
                    <div class="p-top--service-item-icon">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-pink2.svg" alt="">
                    </div>
                  </div>
                </a>
              </div>
              <div class="p-top--service-item pink">
                <a href="" class="link">
                  <div class="p-top--service-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/product02.png" alt="">
                  </div>
                  <div class="p-top--service-item-infor">
                    <div class="p-top--service-item-inner">
                      <p class="p-top--service-item-des">水の質を変えるセラミック</p>
                      <h4 class="p-top--service-item-ttl">QQ Ceramic</h4>
                    </div>
                    <div class="p-top--service-item-icon">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-blue.svg" alt="">
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="p-top--service-row">
            <p class="p-top--service-row-label">
              <img class="pc-only" src="<?php echo $PATH;?>/assets/images/common/title-service.svg" alt="">
              <img class="sp-only" src="<?php echo $PATH;?>/assets/images/common/title-service-sp.png" alt="">
            </p>
            <div class="p-top--service-list">
              <div class="p-top--service-item yellow">
                <div class="title-boxWrap">
                  <div class="ani-border type2">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                  <h3 class="title-box type2">QQを活用したサービス</h3>
                </div>
                <a href="" class="link">
                  <div class="p-top--service-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/service01.png" alt="">
                  </div>
                  <div class="p-top--service-item-infor">
                    <div class="p-top--service-item-inner">
                      <p class="p-top--service-item-des">真の無農薬・無化学栽培</p>
                      <h4 class="p-top--service-item-ttl">QQ&nbsp;<span class="jp">栽培</span></h4>
                    </div>
                    <div class="p-top--service-item-icon">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-blue.svg" alt="">
                    </div>
                  </div>
                </a>
              </div>
              <div class="p-top--service-item blue2">
                <a href="" class="link">
                  <div class="p-top--service-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/service02.png" alt="">
                  </div>
                  <div class="p-top--service-item-infor">
                    <div class="p-top--service-item-inner">
                      <p class="p-top--service-item-des">水・空気・土壌汚染の改善</p>
                      <h4 class="p-top--service-item-ttl">QQ&nbsp;<span class="jp">環境浄化</span></h4>
                    </div>
                    <div class="p-top--service-item-icon">
                      <img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-yellow.svg" alt="">
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </section><!-- ./p-top--service -->
      </div><!-- ./p-service -->
    </div>
  </div>
  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>