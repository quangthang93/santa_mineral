<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>QQ TECHNOLOGYとは</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-tech.png" alt="">
          <span>QQ TECHNOLOGYとは</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-tech">
        <section class="p-tech--intro">
          <div class="p-tech--intro-bg">
            <img src="<?php echo $PATH;?>/assets/images/common/bg-qq2.svg" alt="">
          </div>
          <div class="p-tech--intro-inner">
            <div class="title-boxWrap heading">
              <div class="ani-border type2">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <h3 class="title-box type2">化学依存から脱却し、よりよい地球環境を実現する新技術</h3>
            </div>
            <div class="p-tech--intro-cnt">
              <p class="title-bold">QQテクノロジーは、自然由来のミネラルを抽出、水と融合させることで、<br> 特定の電磁波を発生させる物質を作り出す全く新しい技術です。</p>
              <p class="desc2">化学反応・化学作用の残留性が一切なく、医療・生活環境・農業・工業など多岐の分野での実用化が可能なことから、病気の根本治療や自然環境の再生を可能にする画期的な技術開発につながることで注目を集めています。</p>
              <p class="desc2">QQテクノロジーとは、テラヘルツと人間科学の複合技術です。本テラヘルツテクノロジーを、地球を救う技術という意味を込め「QQテクノロジー」と命名しました。</p>
            </div>
          </div><!-- ./p-tech--intro-inner -->
          <div class="p-tech--intro-img">
            <img src="<?php echo $PATH;?>/assets/images/common/icon-water.svg" alt="">
          </div>
        </section><!-- ./p-tech--intro -->
        <section class="p-tech--function">
          <div class="title-boxWrap heading">
            <div class="ani-border type2">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
            <h3 class="title-box type2">基本性能・機能</h3>
          </div>
          <p class="title-bold">QQテクノロジーの基本性能・機能をご紹介します。</p>
          <div class="p-tech--function-inner">
            <div class="p-tech--function-row01">
              <div class="p-tech--function-row01-img">
                <img src="<?php echo $PATH;?>/assets/images/common/icon-water2.svg" alt="">
              </div>
              <div class="title-boxWrap">
                <div class="ani-border2 type2">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>

                <div class="p-tech--function-row01-cnt">
                  <h3 class="title-quad type2 function">世界初の植物テラヘルツ波機能水</h3>
                  <p class="desc2">QQテクノロジーは、植物や土壌成分からメゾ構造体のミネラルを抽出し、水と融合させることで実用化を可能にしたテラヘルツ技術です。<br> 他の赤外線放射材である電気石(トルマリン)、ゼオライト、希土類、炭等の固形材料と違い、テラヘルツ波を発生させる世界唯一の水性材料と言えます。<br> テラヘルツ波を放射する機能を持つ水を「QQウォーター」と呼びます。</p>
                </div>
              </div>
            </div><!-- ./p-tech--function-row01 -->
            <div class="p-tech--function-row02">
              <div class="p-tech--function-row02-img">
                <img src="<?php echo $PATH;?>/assets/images/common/icon-rock.svg" alt="">
              </div>
              <div class="title-boxWrap">
                <div class="ani-border3 type2">
                  <span></span>
                </div>

                <div class="p-tech--function-row02-cnt">
                  <h3 class="title-quad type2 function">メゾ構造のミネラルを抽出する独自技術</h3>
                  <p class="desc2">サンタミネラルでは、原子のミクロ構造とマクロ構造の中間の世界「メゾスコピック」に注目し、メゾ構造状態の原子には自己起電力があることを発見。メゾ構造のミネラルをさらに水に溶かしていくことで、ミネラルの分散状態にあるテラヘルツ放射体を製造しています。<br>メゾ構造を持つミネラルは自ら電気エネルギーを発することができ、外部からの電力や熱を必要としません。この特有な性質を持ったミネラルは、自らの電気エネルギーを使用することで、対象物に対し有効な働きをもたらします。</p>
                </div>
              </div>
            </div><!-- ./p-tech--function-row02 -->
            <div class="p-tech--function-row03">
              <div class="p-tech--function-row03-img">
                <img src="<?php echo $PATH;?>/assets/images/common/icon-water3.svg" alt="">
              </div>
              <div class="title-boxWrap">
                <div class="ani-border4 type2">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>

                <div class="p-tech--function-row03-cnt">
                  <h3 class="title-quad type2 function">電場の発生</h3>
                  <p class="desc2">当社の製造するテラヘルツ放射体は、ミネラル分散液の形をしているため、極性誘電体の性質を持ちます。<br><br>メゾ構造を持つミネラルはプラスとマイナスの両方を持ち、外界の電子を+極と-極で捕捉、放出することで回路を形成します。この回路ゆえ、自己放電性を持ち電解質を必要とせずに電気分解を行います。</p>
                </div>
              </div>
            </div><!-- ./p-tech--function-row03 -->
            <div class="p-tech--function-row04">
              <div class="p-tech--function-row04-img">
                <img src="<?php echo $PATH;?>/assets/images/common/icon-water4.svg" alt="">
              </div>
              <div class="title-boxWrap">
                <div class="ani-border5 type2">
                  <span></span>
                </div>

                <div class="p-tech--function-row04-cnt">
                  <h3 class="title-quad type2 function">対象物に副作用のないQQテクノロジー</h3>
                  <p class="desc2">テラヘルツ波とは、電波と光の間に位置する電磁波領域で、電波と光の両方の性質を兼ね備えています。その特徴は、光のように一定方向に進み、互いに波長が合うことで、特定の分子や分子間の運動に吸収され、物質を様々な形で活性化させる作用があるということです。<br>テラヘルツ波を放射する機能を持つ「QQウォーター」は、水とミネラルだけで構成されているので、対象物への化学反応や化学作用などの残留性は一切ありません。QQテクノロジーは自然界が行っている現象を作り出しているのであり、逆に言うと、自然界が行わない作用を作り出すことは、当社としては行わない方針です。</p>
                </div>
              </div>
            </div><!-- ./p-tech--function-row04 -->
          </div><!-- ./p-tech--function-inner -->
        </section><!-- ./p-tech--function -->
        <section class="p-tech--activity">
          <div class="title-boxWrap heading">
            <div class="ani-border type2">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
            <h3 class="title-box type2">活用領域</h3>
          </div>
          <p class="title-bold">QQテクノロジーは、医療・生活環境・農業・工業など多岐の分野での実用化が可能です。</p>
          <div class="p-tech--activity-inner">
            <div class="p-tech--activity-row">
              <div class="p-tech--activity-left">
                <div class="_img">
                  <h4 class="_ttl">
                    <img src="<?php echo $PATH;?>/assets/images/end/technology/ttl01.png" alt="生活環境">
                  </h4>
                  <img src="<?php echo $PATH;?>/assets/images/end/technology/icon-activity01.png" alt="">
                </div>
              </div>
              <div class="p-tech--activity-right">
                <div class="p-tech--activity-right-list">
                  <div class="p-tech--activity-right-item">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">除菌・消毒(テラ・プロテクト)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">高アルカリ性と電気の相乗効果で細菌、ウイルス、微生物を99.99％除去。自然由来の成分のため、アレルギー反応もありません。新型コロナウイルスの不活性化も確認されています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">洗浄(テラ・ヘルパー)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">臭気分解(テラ・シュシュ)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">水道水殺菌(プロテクト・ストーン)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4">燃焼効率の向上(テラ・エフェクト)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                </div><!-- ./p-tech--activity-right-list -->
              </div><!-- ./p-tech--activity-right -->
            </div><!-- ./p-tech--activity-row -->
            <div class="p-tech--activity-row">
              <div class="p-tech--activity-left type2">
                <div class="_img">
                  <h4 class="_ttl">
                    <img src="<?php echo $PATH;?>/assets/images/end/technology/ttl02.png" alt="美容・健康">
                  </h4>
                  <img src="<?php echo $PATH;?>/assets/images/end/technology/icon-activity02.png" alt="">
                </div>
              </div>
              <div class="p-tech--activity-right">
                <div class="p-tech--activity-right-list">
                  <div class="p-tech--activity-right-item type2">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">肌質改善(テラ・ピュア)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">高アルカリ性と電気の相乗効果で細菌、ウイルス、微生物を99.99％除去。自然由来の成分のため、アレルギー反応もありません。新型コロナウイルスの不活性化も確認されています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item type2">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">身体活性(テラ・サポート)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item type2">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">頭皮ケア(テラ・ピュアH)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                </div><!-- ./p-tech--activity-right-list -->
              </div><!-- ./p-tech--activity-right -->
            </div><!-- ./p-tech--activity-row -->
            <div class="p-tech--activity-row">
              <div class="p-tech--activity-left type3">
                <div class="_img">
                  <h4 class="_ttl">
                    <img src="<?php echo $PATH;?>/assets/images/end/technology/ttl03.png" alt="農業・畜産">
                  </h4>
                  <img src="<?php echo $PATH;?>/assets/images/end/technology/icon-activity03.png" alt="">
                </div>
              </div>
              <div class="p-tech--activity-right">
                <div class="p-tech--activity-right-list">
                  <div class="p-tech--activity-right-item type3">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">無農薬栽培(QQ栽培)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">高アルカリ性と電気の相乗効果で細菌、ウイルス、微生物を99.99％除去。自然由来の成分のため、アレルギー反応もありません。新型コロナウイルスの不活性化も確認されています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item type3">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">家畜の発育改善(QQセラミック)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                </div><!-- ./p-tech--activity-right-list -->
              </div><!-- ./p-tech--activity-right -->
            </div><!-- ./p-tech--activity-row -->
            <div class="p-tech--activity-row">
              <div class="p-tech--activity-left type4">
                <div class="_img">
                  <h4 class="_ttl">
                    <img src="<?php echo $PATH;?>/assets/images/end/technology/ttl04.png" alt="農業・畜産">
                  </h4>
                  <img src="<?php echo $PATH;?>/assets/images/end/technology/icon-activity04.png" alt="">
                </div>
              </div>
              <div class="p-tech--activity-right">
                <div class="p-tech--activity-right-list">
                  <div class="p-tech--activity-right-item type4">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">水質浄化(QQ環境浄化)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">高アルカリ性と電気の相乗効果で細菌、ウイルス、微生物を99.99％除去。自然由来の成分のため、アレルギー反応もありません。新型コロナウイルスの不活性化も確認されています。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item type4">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">土壌汚染の改善(QQ環境浄化)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                  <div class="p-tech--activity-right-item type4">
                    <div class="p-tech--activity-right-inner">
                      <div class="p-tech--activity-right-head">
                        <h5 class="title-lv4 js-equalTtlLv4">プラスチック分解(QQ環境浄化)</h5>
                      </div>
                      <div class="p-tech--activity-right-cnt">
                        <p class="desc2 js-equalDesc">このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。このテキストはダミーです。</p>
                        <a href="" class="link-border"><span>詳しくみる</span></a>
                      </div>
                    </div>
                  </div><!-- ./p-tech--activity-right-item -->
                </div><!-- ./p-tech--activity-right-list -->
              </div><!-- ./p-tech--activity-right -->
            </div><!-- ./p-tech--activity-row -->
          </div><!-- ./p-tech--activity-inner -->
        </section><!-- ./p-tech--activity -->
      </div><!-- ./p-tech -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>