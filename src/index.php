<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main p-top">
  <section class="mv">
    <div class="mv-ttl--wrap">
      <div class="mv-ttl--inner">
        <h2 class="mv-ttl">
          <img class="pc-only" src="<?php echo $PATH;?>/assets/images/common/title-mv.svg" alt="Santa Mineral （サンタミネラル）">
          <img class="sp-only" src="<?php echo $PATH;?>/assets/images/common/title-mv-sp.svg" alt="Santa Mineral （サンタミネラル）">
        </h2>
        <div class="mv-cloud sp-only2 fadeleft delay-20 js-mvCloud">
          <img src="<?php echo $PATH;?>/assets/images/common/cloud.png" alt="">
        </div>
      </div>
    </div><!-- ./mv-ttl--wrap -->
    <div class="mv-bgWrap">
      <!-- <div class="mv-bg">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-mv.svg" alt="">
      </div> -->
      <div class="mv-magicWrap">
        <div class="mv-magic island fuwafuwa">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_island.svg" alt="">
        </div>
        <div class="mv-magic cloud3 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud3.svg" alt="">
        </div>
        <div class="mv-magic cloud8 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud8.svg" alt="">
        </div>
        <div class="mv-magic cloud10 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud10.svg" alt="">
        </div>
        <div class="mv-magic cloud11 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud11.svg" alt="">
        </div>
        <div class="mv-magic dog pikopiko2">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_dog.svg" alt="">
        </div>
        <div class="mv-magic balloon fuwafuwa2">
          <img src="<?php echo $PATH;?>/assets/images/top/amine_balloon.svg" alt="">
        </div>
        <div class="mv-magic woman pikopiko2">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_woman.svg" alt="">
        </div>
        <div class="mv-magic fish fuwafuwa">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_fish.svg" alt="">
        </div>
        <div class="mv-magic girl pikopiko">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_girl.svg" alt="">
        </div>
        <div class="mv-magic boy pikopiko">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_boy.svg" alt="">
        </div>
        <div class="mv-magic bird fuwafuwa2">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_bird.svg" alt="">
        </div>
        <div class="mv-magic bear fuwafuwa3">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_bear.svg" alt="">
        </div>
        <div class="mv-magic cloud9 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud9.svg" alt="">
        </div>
        <div class="mv-magic bird7 fuwafuwa2">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_bird7.svg" alt="">
        </div>
        <div class="mv-magic cloud12 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud12.svg" alt="">
        </div>
        <div class="mv-magic cloud13 fuwafuwa-cloud">
          <img src="<?php echo $PATH;?>/assets/images/top/anime_cloud13.svg" alt="">
        </div>
      </div>
    </div>
    <div class="scroll js-scroll">
      <a href="javascript:void(0)">PLEASE SCROLL</a>
    </div>
  </section><!-- ./mv -->

  <div class="p-top--intro">
    <div class="p-top--intro-cnt">
      <div class="p-top--intro-box1">
        <div class="ani-border">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <p class="p-top--intro-box1-infor">
          自然由来のチカラを使った新しい技術で 持続可能な社会を創っていきます。
        </p>
      </div>
      <div class="p-top--intro-box2Wrap">
        <div class="p-top--intro-box2">
          <div class="ani-border">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <p class="p-top--intro-box2-infor">
            私たちは、テラヘルツ機能材料及びその応用製品の開発・製造・ 技術提供を行う企業です。 当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、 <br>対象物に対し有効な働きをもたらします。 <br>この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、 あらゆる産業に応用が可能です。
          </p>
        </div>
		    <div class="p-top--intro-box2Wrap-direct">
		      <a class="link-pink faderight" href=""><span>当社について</span></a>
		    </div>
      </div>
    </div>
  </div><!-- ./p-top--intro -->

  <section class="p-top--technology fadeup">
  	<!-- <h3 class="p-top--technology-ttl">
  		 <img src="<?php echo $PATH;?>/assets/images/common/title-technology.svg" alt="technology">
  	</h3> -->
  	<div class="p-top--technology-cnt">
  		<div class="p-top--technology-cnt-infor">
  			<div class="title-boxWrap">
  				<div class="ani-border type2">
	          <span></span>
	          <span></span>
	          <span></span>
	          <span></span>
	        </div>
  				<h3 class="title-box type2">自然由来のミネラルで世界を救う</h3>
  			</div>
  			<p class="desc">QQテクノロジーは、自然由来のミネラルを抽出、水と融合させることで、<br class="pc-only"> 特定の電磁波を発生させる物質を作り出す全く新しい技術です。 <br>この技術は、対象物に対し有効な働きをもたらし、 <br class="pc-only">病気の根本治療や自然環境の再生を可能にする画期的な技術開発につながることで、 <br class="pc-only">世界的に注目を集めています。</p>
  		</div>
  		<div class="p-top--technology-cnt-img">
  			<img src="<?php echo $PATH;?>/assets/images/common/icon-water.svg" alt="">
  		</div>
  	</div><!-- ./p-top--technology-cnt -->
  	<div class="p-top--technology-direct">
  		<a class="link-pink faderight" href=""><span>QQ TECHNOLOGYとは</span></a>
  	</div><!-- ./p-top--technology-direct -->
  </section><!-- ./p-top--technology -->

  <section class="p-top--service">
  	<div class="p-top--service-row fadeup">
  		<p class="p-top--service-row-label">
  			<img class="pc-only" src="<?php echo $PATH;?>/assets/images/common/title-product.svg" alt="">
        <img class="sp-only" src="<?php echo $PATH;?>/assets/images/common/title-product-sp.png" alt="">
  		</p>
  		<div class="p-top--service-list">
  			<div class="p-top--service-item blue">
  				<div class="title-boxWrap">
	  				<div class="ani-border type2">
		          <span></span>
		          <span></span>
		          <span></span>
		          <span></span>
		        </div>
	  				<h3 class="title-box type2">QQの技術を応用した製品</h3>
	  			</div>
  				<a href="" class="link">
  					<div class="p-top--service-item-thumb">
  						<img class="cover" src="<?php echo $PATH;?>/assets/images/common/product01.png" alt="">
  					</div>
  					<div class="p-top--service-item-infor">
  						<div class="p-top--service-item-inner">
  							<p class="p-top--service-item-des">世界初の植物テラヘルツ波機能水</p>
  							<h4 class="p-top--service-item-ttl">QQ Water</h4>
  						</div>
  						<div class="p-top--service-item-icon">
  							<img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-pink2.svg" alt="">
  						</div>
  					</div>
  				</a>
  			</div>
  			<div class="p-top--service-item pink">
  				<a href="" class="link">
  					<div class="p-top--service-item-thumb">
  						<img class="cover" src="<?php echo $PATH;?>/assets/images/common/product02.png" alt="">
  					</div>
  					<div class="p-top--service-item-infor">
  						<div class="p-top--service-item-inner">
  							<p class="p-top--service-item-des">水の質を変えるセラミック</p>
  							<h4 class="p-top--service-item-ttl">QQ Ceramic</h4>
  						</div>
  						<div class="p-top--service-item-icon">
  							<img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-blue.svg" alt="">
  						</div>
  					</div>
  				</a>
  			</div>
  		</div>
  	</div>
  	<div class="p-top--service-row fadeup">
  		<p class="p-top--service-row-label">
        <img class="pc-only" src="<?php echo $PATH;?>/assets/images/common/title-service.svg" alt="">
        <img class="sp-only" src="<?php echo $PATH;?>/assets/images/common/title-service-sp.png" alt="">
  		</p>
  		<div class="p-top--service-list" data-cmt="QQテクノロジーはいろいろな用途に応用可能で、カスタマイズもできます。">
  			<div class="p-top--service-item yellow">
  				<div class="title-boxWrap">
	  				<div class="ani-border type2">
		          <span></span>
		          <span></span>
		          <span></span>
		          <span></span>
		        </div>
	  				<h3 class="title-box type2">QQを活用したサービス</h3>
	  			</div>
  				<a href="" class="link">
  					<div class="p-top--service-item-thumb">
  						<img class="cover" src="<?php echo $PATH;?>/assets/images/common/service01.png" alt="">
  					</div>
  					<div class="p-top--service-item-infor">
  						<div class="p-top--service-item-inner">
  							<p class="p-top--service-item-des">真の無農薬・無化学栽培</p>
  							<h4 class="p-top--service-item-ttl">QQ&nbsp;<span class="jp">栽培</span></h4>
  						</div>
  						<div class="p-top--service-item-icon">
  							<img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-blue.svg" alt="">
  						</div>
  					</div>
  				</a>
  			</div>
  			<div class="p-top--service-item blue2">
  				<a href="" class="link">
  					<div class="p-top--service-item-thumb">
  						<img class="cover" src="<?php echo $PATH;?>/assets/images/common/service02.png" alt="">
  					</div>
  					<div class="p-top--service-item-infor">
  						<div class="p-top--service-item-inner">
  							<p class="p-top--service-item-des">水・空気・土壌汚染の改善</p>
  							<h4 class="p-top--service-item-ttl">QQ&nbsp;<span class="jp">環境浄化</span></h4>
  						</div>
  						<div class="p-top--service-item-icon">
  							<img src="<?php echo $PATH;?>/assets/images/common/icon-arrowbig-yellow.svg" alt="">
  						</div>
  					</div>
  				</a>
  			</div>
  		</div>
  	</div>
  	<div class="p-top--service-direct">
  		<a class="link-pink faderight" href=""><span>技術・製品情報</span></a>
  	</div><!-- ./p-top--service-direct -->
  </section><!-- ./p-top--service -->

  <section class="p-top--news fadeup">
  	<div class="p-top--news-inner">
  		<h3 class="title-quad">NEWS / REPORTS</h3>
  		<ul class="p-top--news-list js-newsSliders">
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">プレスリリース</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
							</div>
						</div>
					</a>
				</li>
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">お知らせ</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園...</p>
							</div>
						</div>
					</a>
				</li>
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">プレスリリース</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news04.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
							</div>
						</div>
					</a>
				</li>
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">導入事例</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news05.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
							</div>
						</div>
					</a>
				</li>
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">レポート</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
							</div>
						</div>
					</a>
				</li>
				<li class="p-top--news-item">
					<a href="" class="link">
						<div class="p-top--news-item-thumbWrap">
							<span class="label type2">プレスリリース</span>
							<div class="p-top--news-item-thumb">
								<img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
							</div>
							<div class="p-top--news-item-cnt">
								<p class="date">2021.03.21</p>
								<p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
							</div>
						</div>
					</a>
				</li>
  		</ul>
  		<div class="viewmoreWap">
  			<a href="" class="viewmore">一覧を見る</a>
  		</div>
  	</div>
  </section><!-- ./p-top--news -->

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>