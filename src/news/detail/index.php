<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>ニュース・レポート</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-news.png" alt="">
          <span>ニュース・レポート</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-news">
        <div class="p-news--cnt">
          <div class="p-news--detail">
            <div class="p-news--detail-dateWrap align-center">
              <p class="date2 mgb-10">2021.03.21</p>
              <span class="label2">プレスリリース</span>
            </div>
            <div class="p-news--detail-cnt no-reset">
              <h2>島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</h2>
              <img class="mgb-50" src="<?php echo $PATH;?>/assets/images/end/news/news01.png" alt="">
              <h3>こちらに中見出しが入ります</h3>
              <p>私たちは、<a href="">テラヘルツ機能材料</a>及びその応用製品の開発・製造・ 技術提供を行う企業です。当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、対象物に対し有効な働きをもたらします。この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、あらゆる産業に応用が可能です。私たちは、テラヘルツ機能材料及びその応用製品の開発・製造・ 技術提供を行う企業です。当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、対象物に対し有効な働きをもたらします。この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、あらゆる産業に応用が可能です。</p>
              <br>
              <a href="" class="link-icon arrow mgb-20">テキストリンク</a>
              <br>
              <a href="" class="link-icon pdf mgb-20">PDFリンク</a>
              <br>
              <a href="" class="link-icon blank mgb-20">外部リンク</a>
              <br>
              <ul>
                <li>こちらにリストが入ります</li>
                <li>こちらにリストが入ります</li>
                <li>こちらにリストが入ります</li>
              </ul>
              <br>
              <br>
              <br>
              <div class="iframe">
                <iframe width="900" height="506" src="https://www.youtube.com/embed/6pxRHBw-k8M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p class="iframe-ttl">こちらにムービータイトル(キャプション)が入ります</p>
              </div>
              <br>
              <br>
              <h3>こちらに中見出しが入ります</h3>
              <br>
              <div class="col2-55">
                <div class="col2-55--left">
                  <h4>こちらに小見出しが入ります</h4>
                  <p>この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                </div>
                <div class="col2-55--right">
                  <div class="thumb">
                    <img src="/assets/images/end/news/news03.png" alt="">
                  </div>
                </div>
              </div>
              <br>
              <div class="col2-45">
                <div class="col2-45--left">
                  <div class="thumb">
                    <img src="/assets/images/end/news/news04.png" alt="">
                  </div>
                </div>
                <div class="col2-45--right">
                  <h4>こちらに小見出しが入ります</h4>
                  <p>この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーテキストです。お読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                </div>
              </div>
              <br>
              <div class="col2">
                <div class="col2-item">
                  <div class="thumb">
                    <img src="/assets/images/end/news/news05.png" alt="">
                  </div>
                  <p class="thumb-ttl">こちらにキャプションが入ります</p>
                </div>
                <div class="col2-item">
                  <div class="thumb">
                    <img src="/assets/images/end/news/news06.png" alt="">
                  </div>
                  <p class="thumb-ttl">こちらにキャプションが入ります</p>
                </div>
              </div>
            </div>
            <div class="align-center mgt-70">
              <a href="/news" class="viewmore2">一覧へ戻る</a>
            </div>
          </div><!-- /.p-news--detail -->
        </div><!-- /.p-news--cnt -->
        <div class="p-news--sidebar">
          <p class="title-bold mgb-20">カテゴリー</p>
          <ul class="p-news--sidebar-list">
            <li class="link active"><a href="">すべて</a></li>
            <li class="link"><a href="">お知らせ</a></li>
            <li class="link"><a href="">セミナー・イベント</a></li>
            <li class="link"><a href="">メディア掲載情報</a></li>
            <li class="link"><a href="">製品情報</a></li>
            <li class="link"><a href="">導入事例</a></li>
            <li class="link"><a href="">レポート</a></li>
            <li class="link"><a href="">その他</a></li>
          </ul>
          <p class="title-bold mgb-20">過去のお知らせ</p>
          <select name="pref" class="select">
            <option value="" selected="selected">
              掲載年で絞り込む </option>
            <option value="2019">
              2019 </option>
            <option value="2020">
              2020 </option>
              <option value="2021">
              2021 </option>
          </select>
        </div><!-- .p-news--sidebar -->
      </div><!-- ./p-news -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>