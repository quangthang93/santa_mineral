<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>ニュース・レポート</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-news.png" alt="">
          <span>ニュース・レポート</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-news">
        <div class="p-news--cnt">
          <ul class="p-top--news-list">
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">お知らせ</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news04.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">導入事例</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news05.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">レポート</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb video">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">お知らせ</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news04.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">導入事例</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news05.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">レポート</span>
                  <div class="p-top--news-item-thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news01.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
            <li class="p-top--news-item">
              <a href="/news/detail" class="link">
                <div class="p-top--news-item-thumbWrap">
                  <span class="label type2">プレスリリース</span>
                  <div class="p-top--news-item-thumb video">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/news03.png" alt="">
                  </div>
                  <div class="p-top--news-item-cnt">
                    <p class="date">2021.03.21</p>
                    <p class="p-top--news-item-ttl">島根県益田市・大村公園の底質ヘドロの分解及び水質改善の取り組み</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
          <div class="pagination">
            <p class="pagination-label">0,000件中｜1〜30件 表示</p>
            <div class="pagination-list">
              <a class="ctrl prev" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="8" viewBox="0 0 4 8"><path d="M498.4,614.982l4-4v1.758l-2.242,2.242,2.242,2.242v1.758Z" transform="translate(-498.404 -610.982)"/></svg>
              </a>
              <a class="active" href="">1</a>
              <a href="">2</a>
              <a href="">3</a>
              <a href="">4</a>
              <a href="">5</a>
              <a href="">6</a>
              <div class="pagination-spacer">…</div>
              <a href="">12</a>
              <a class="ctrl next" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="8" viewBox="0 0 4 8"><path d="M502.4,614.982l-4-4v1.758l2.242,2.242-2.242,2.242v1.758Z" transform="translate(-498.404 -610.982)"/></svg>
              </a>
            </div>
          </div>
        </div><!-- /.p-news--cnt -->
        <div class="p-news--sidebar">
          <p class="title-bold mgb-20">カテゴリー</p>
          <ul class="p-news--sidebar-list">
            <li class="link active"><a href="">すべて</a></li>
            <li class="link"><a href="">お知らせ</a></li>
            <li class="link"><a href="">セミナー・イベント</a></li>
            <li class="link"><a href="">メディア掲載情報</a></li>
            <li class="link"><a href="">製品情報</a></li>
            <li class="link"><a href="">導入事例</a></li>
            <li class="link"><a href="">レポート</a></li>
            <li class="link"><a href="">その他</a></li>
          </ul>
          <p class="title-bold mgb-20">過去のお知らせ</p>
          <select name="pref" class="select">
            <option value="" selected="selected">
              掲載年で絞り込む </option>
            <option value="2019">
              2019 </option>
            <option value="2020">
              2020 </option>
              <option value="2021">
              2021 </option>
          </select>
        </div><!-- .p-news--sidebar -->
      </div><!-- ./p-news -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>