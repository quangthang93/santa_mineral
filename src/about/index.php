<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>当社について</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-about.png" alt="">
          <span>当社について</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-about">
        <div class="p-top--intro">
          <div class="p-top--intro-cnt">
            <div class="p-top--intro-box1">
              <div class="ani-border">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <p class="p-top--intro-box1-infor">
                次世代の持続可能な開発のために、<br>地球環境の本来のチカラを取り戻す。
              </p>
            </div>
            <div class="p-top--intro-box2Wrap">
              <div class="p-top--intro-box2">
                <div class="ani-border">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
                <div class="p-top--intro-box2-infor">
                  <p>株式会社Santa Mineralは、自然界にあるミネラルのメゾ構造を維持させ、水と融合させた世界初の新技術 「QQテクノロジー」のテラヘルツ機能材料及びその応用製品の開発・製造及び販売を行う会社です。</p>
                  <p>当社は、環境問題及び化学物質依存システムからの脱却を目指す物理学的新技術「テラヘルツの水性化技術 - QQテクノロジー」の事業化を目指し、2013年6月に設立。QQテクノロジーは、一般社団法人ミネラル活性化技術研究所に所属する各分野の研究者約35名の知恵と経験から生み出されました。</p>
                  <div class="p-top--intro-box2-2col">
                    <div class="p-top--intro-box2-2col-left">
                      <p>QQテクノロジーの最大の特徴は、全て自然界にある植物ミネラルのメゾスコピック構造を源泉としており、「人畜無害・無化学」であるということです。その性質から医療・生活環境・農業、・業など、多岐の分野で実用化が可能なところです。</p>
                      <p>人々を化学システムの依存から脱却させ、次世代のためにより良い世界を築いていくことが、当社の目標であり、果たすべき義務であると考えています。</p>
                    </div>
                    <div class="p-top--intro-box2-2col-right">
                      <img src="<?php echo $PATH;?>/assets/images/end/about/logo_about.png" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="p-about--cnt">
          <h2 class="title-lv2">当社の強み</h2>
          <div class="p-about--cnt-list">
            <div class="p-about--cnt-item">
              <h3 class="title-lv3">QQテクノロジーの技術の特性</h3>
              <div class="p-about--cnt-item-inner">
                <div class="p-about--cnt-item-des">
                  <p class="desc2 mgb-10">QQテクノロジーは、植物や土壌成分からメゾ構造体のミネラルを抽出し、水と融合させることで実用化を可能にしたテラヘルツ技術です。他の赤外線放射材である電気石(トルマリン)、ゼオライト、希土類、炭等の固形材料と違い、テラヘルツ波を発生させる世界唯一の水性材料と言えます。</p>
                  <p class="desc2">在野の研究所によって紡ぎ出されたこの世界的な技術は、東京大学など多くの大学、研究機関で証明され、学会発表などを通して広く認知されています。</p>
                  <a href="" class="link-border"><span>QQ TECHNOLOGYとは</span></a>
                </div>
                <div class="p-about--cnt-item-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/end/about/about01.png" alt="">
                </div>
              </div>
            </div><!-- ./p-about--cnt-item -->
            <div class="p-about--cnt-item">
              <h3 class="title-lv3">QQテクノロジーは、様々な分野で実用化が期待されています</h3>
              <div class="p-about--cnt-item-inner">
                <div class="p-about--cnt-item-des">
                  <p class="desc2 mgb-10">QQテクノロジーの最大の特徴は、全て自然界にある植物ミネラルのメゾスコピック構造を源泉としており、「人畜無害・無化学」であるということです。その性質から医療・生活環境・農業、・業など、多岐の分野で実用化が可能なところです。</p>
                  <p class="desc2">水とミネラルだけで構成されているので、対象物への化学反応や化学作用などの残留性は一切ありません。QQ テクノロジーは自然界が行っている現象を作り出しているのであり、逆に言うと、自然界が行わない作用を作り出すことは、弊社としては行わない方針です。</p>
                  <a href="" class="link-border"><span>技術・製品情報</span></a>
                </div>
                <div class="p-about--cnt-item-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/end/about/about02.png" alt="">
                </div>
              </div>
            </div><!-- ./p-about--cnt-item -->
            <div class="p-about--cnt-item">
              <h3 class="title-lv3">共同研究による、QQテクノロジーのさらなる可能性</h3>
              <div class="p-about--cnt-item-inner">
                <div class="p-about--cnt-item-des">
                  <p class="desc2 mgb-10">QQテクノロジーの技術は、実用化に向けて、実証実験や研究が進められています。</p>
                  <p class="desc2">東京大学　持続可能な自然再生科学研究室と共同で「水棲環境、微生物および動植物の生活環境の改善・無農薬栽培」について研究を進めている他、島根県益田市の協力のもと、「益田モデル」として、水質浄化や松がれ対策など、地域環境の改善をめざす実証実験を行っています。</p>
                  <!-- <a href="" class="link-border"><span>技術・製品情報</span></a> -->
                </div>
                <div class="p-about--cnt-item-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/end/about/about03.png" alt="">
                </div>
              </div>
            </div><!-- ./p-about--cnt-item -->
            <div class="p-about--cnt-item">
              <h3 class="title-lv3">SDGsに対する当社の考え</h3>
              <div class="p-about--cnt-item-inner">
                <div class="p-about--cnt-item-des">
                  <p class="desc2 mgb-10">SDGs(持続可能な開発目標)は、2015年9月の国連サミットで採択された国際目標です。2030年までに持続可能でよりよい世界を目指し、17のゴール・169のターゲットから構成され，地球上の「誰一人取り残さない」ことを誓っています。</p>
                  <p class="desc2 mgb-10">SDGs(持続可能な開発目標)QQテクノロジーは、SDGsの達成に大きく貢献します。</p>
                  <p class="desc2">サンタミネラルでは、特に「6. 安全な水とトイレを世界中に」「14. 海の豊かさを守ろう」「15. 陸の豊かさも守ろう」の3項目に着目し、地球に存在する様々な課題の根本的解決を目指します。</p>
                  <a href="" class="link-border"><span>SDGｓへの取り組み</span></a>
                </div>
                <div class="p-about--cnt-item-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/end/about/about04.png" alt="">
                </div>
              </div>
            </div><!-- ./p-about--cnt-item -->
          </div><!-- ./p-about--cnt-list -->
        </div>
      </div><!-- ./p-about -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>