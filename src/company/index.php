<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>会社情報</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-company.png" alt="">
          <span>会社情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-company">
        <div class="tabs">
          <div class="tabs-navWrapper">
            <div class="tabs-nav">
              <a class="tabs-item active" href="/company">ごあいさつ</a>
              <a class="tabs-item" href="/company/profile/">会社概要</a>
              <a class="tabs-item" href="/company/patent/">保有特許</a>
            </div>
          </div>
          <div class="tabs-cnt">
            <div class="tabs-panel type2">
              <h2 class="title-lv2-2">化学システムの依存からの脱却と<br> よりよい世界をめざして</h2>
              <div class="p-company--intro">
                <p class="desc3 mgb-20">株式会社Santa Mineralは、環境問題及び化学物質依存システムからの脱却を目指す物理学的新技術「テラヘルツの水性化技術 - QQテクノロジー」の事業化を目指し、2013年6月に設立。弊社のQQテクノロジーは、35年の歳月を経て、一般社団法人ミネラル活性化技術研究所に所属する各分野の研究者約35名の知恵と経験から生み出されました。彼らは、理論だけでなく、理論から現物を作り出すという偉業を成し遂げた集団です。このテクノロジーは、一切の化学物質を使用しておらず、自然界にあるミネラルのメゾ構造を維持させ、水と融合させた、世界初の技術です。人々を化学システムの依存から脱却させ、次世代のためにより良い世界を築いていくことが、弊社の目標であり、果たすべき義務であると考えています。</p>
                <h4 class="title-lv4 mgb-20">化学システムの依存からの脱却とよりよい世界をめざして</h4>
                <p class="desc3">本文パターン1：lineheight1.8＞私たちは、<a href="" class="link-pink2">テラヘルツ機能材料</a>及びその応用製品の開発・製造・ 技術提供を行う企業です。当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、対象物に対し有効な働きをもたらします。この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、あらゆる産業に応用が可能です。本文パターン1：lineheight1.8＞私たちは、テラヘルツ機能材料及びその応用製品の開発・製造・ 技術提供を行う企業です。当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、対象物に対し有効な働きをもたらします。この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、あらゆる産業に応用が可能です。</p>

              </div>
              <div class="p-company--sign">
                <p class="p-company--sign-label">株式会社Santa Mineral 代表取締役</p>
                <p class="p-company--sign-img">
                  <img src="/assets/images/end/company/sign.png" alt="">
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>