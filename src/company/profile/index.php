<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>会社情報</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-company.png" alt="">
          <span>会社情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-company">
        <div class="tabs">
          <div class="tabs-navWrapper">
            <div class="tabs-nav">
              <a class="tabs-item" href="/company">ごあいさつ</a>
              <a class="tabs-item active" href="/company/profile/">会社概要</a>
              <a class="tabs-item" href="/company/patent/">保有特許</a>
            </div>
          </div>
          <div class="tabs-cnt">
            <div class="tabs-panel">
              <div class="p-company--infor">
                <ul class="anchor--list">
                  <li>
                    <a href="#profile" class="link-anchor">会社概要</a>
                  </li>
                  <li>
                    <a href="#listOfBases" class="link-anchor">拠点一覧</a>
                  </li>
                </ul><!-- .p-company--infor-bar -->
                <div class="p-company--infor-cnt">
                  <h2 class="title-lv2 mgb-60" id="profile">会社概要</h2>
                  <div class="table">
                    <table class="mgb-100">
                      <tr>
                        <th>商号</th>
                        <td>株式会社Santa Mineral (Santa Mineral Co., Ltd.)</td>
                      </tr>
                      <tr>
                        <th>設立</th>
                        <td>平成25年6月18日</td>
                      </tr>
                      <tr>
                        <th>代表者</th>
                        <td>代表取締役　太西るみ子</td>
                      </tr>
                      <tr>
                        <th>資本金</th>
                        <td>10,000,000円 （20xx年xx月xx日現在）</td>
                      </tr>
                      <tr>
                        <th>本社所在地</th>
                        <td>〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号［<a href="https://goo.gl/maps/AhA5di9ir9ZUcchw7" target="_blank" class="link-icon blank">Google Map</a>］ <br>TEL 03-6450-1905　FAX 076-282-9158</td>
                      </tr>
                      <tr>
                        <th>事業内容</th>
                        <td>テラヘルツ放射機能材料及びその応用製品の開発、製造及び卸し その他の事業</td>
                      </tr>
                      <tr>
                        <th>主な取引先</th>
                        <td>
                          一般社団法人ミネラル活性化技術研究所<br>
                          一般社団法人ミネラル活性化技術研究所<br>
                          一般社団法人ミネラル活性化技術研究所<br>
                          一般社団法人ミネラル活性化技術研究所
                        </td>
                      </tr>
                      <tr>
                        <th>関連会社・団体</th>
                        <td>一般社団法人ミネラル活性化技術研究所</td>
                      </tr>
                    </table>
                  </div>
                  <h2 class="title-lv2 mgb-60" id="listOfBases">拠点一覧</h2>
                  <h3 class="title-lv3 mgb-30">本社</h3>
                  <p class="desc mgb-50">〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号 <br>TEL 03-6450-1905 FAX 076-282-9158</p>

                  <h3 class="title-lv3 mgb-30">益田営業所</h3>
                  <p class="desc mgb-50">〒698-0024 島根県益田市​駅前町17-1　益田駅前ビルEAGA 2Ｆ <br>TEL 000-0000-0000 FAX 000-0000-0000</p>

                  <h3 class="title-lv3 mgb-30">金沢営業所</h3>
                  <p class="desc">〒920-3121 石川県金沢市大場町東840 <br>TEL 000-0000-0000 FAX 000-0000-0000</p>
                </div><!-- .p-company--infor-cnt -->
              </div><!-- .p-company--infor -->
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>