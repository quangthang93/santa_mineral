<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">
  <div class="p-end--cnt">
    <div class="container">
      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>会社情報</li>
            <li>会社情報</li>
            <li>会社情報</li>
            <li>会社情報</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->
      <section class="p-end--banner">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-company.png" alt="">
          <span>会社情報</span>
        </h1>
      </section><!-- ./p-recruit--banner -->
      <div class="p-company">
        <div class="tabs">
          <div class="tabs-navWrapper">
            <div class="tabs-nav">
              <a class="tabs-item" href="/company">ごあいさつ</a>
              <a class="tabs-item" href="/company/profile/">会社概要</a>
              <a class="tabs-item active" href="/company/patent/">保有特許</a>
            </div>
          </div>
          <div class="tabs-cnt">
            <div class="tabs-panel">
              <p class="title-bold mgb-30">現在、日本、米国、中国、台湾で20以上の特許を申請中です。</p>
              <div class="p-company--table table custom-width02">
                <table>
                  <thead>
                    <tr>
                      <th>権利国</th>
                      <th>権利国</th>
                      <th>発明の名称</th>
                      <th>発明者</th>
                      <th>特許権者</th>
                      <th>ステータス</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td data-label="権利国">特許第6030270号 (2016年9月16日)</td>
                      <td data-label="権利国">日本</td>
                      <td data-label="発明の名称">ミネラル機能水の製造方法</td>
                      <td data-label="発明者">古崎　孝一</td>
                      <td data-label="特許権者">株式会社理研テクノシステム 株式会社Santa Mineral</td>
                      <td data-label="ステータス"><a href="" target="_blank" class="link-icon pdf">申請済</a></td>
                    </tr>
                    <tr>
                      <td data-label="権利国">特許第6030270号 (2016年9月16日)</td>
                      <td data-label="権利国">日本</td>
                      <td data-label="発明の名称">ミネラル機能水の製造方法</td>
                      <td data-label="発明者">古崎　孝一</td>
                      <td data-label="特許権者">株式会社理研テクノシステム 株式会社Santa Mineral</td>
                      <td data-label="ステータス"><a href="" target="_blank" class="link-icon pdf">申請済</a></td>
                    </tr>
                    <tr>
                      <td data-label="権利国">特許第6030270号 (2016年9月16日)</td>
                      <td data-label="権利国">日本</td>
                      <td data-label="発明の名称">ミネラル機能水の製造方法</td>
                      <td data-label="発明者">古崎　孝一</td>
                      <td data-label="特許権者">株式会社理研テクノシステム 株式会社Santa Mineral</td>
                      <td data-label="ステータス"><a href="" target="_blank" class="link-icon pdf">申請済</a></td>
                    </tr>
                    <tr>
                      <td data-label="権利国">特許第6030270号 (2016年9月16日)</td>
                      <td data-label="権利国">日本</td>
                      <td data-label="発明の名称">ミネラル機能水の製造方法</td>
                      <td data-label="発明者">古崎　孝一</td>
                      <td data-label="特許権者">株式会社理研テクノシステム 株式会社Santa Mineral</td>
                      <td data-label="ステータス"><a href="" target="_blank" class="link-icon pdf">申請済</a></td>
                    </tr>
                    <tr>
                      <td data-label="権利国">特許第6030270号 (2016年9月16日)</td>
                      <td data-label="権利国">日本</td>
                      <td data-label="発明の名称">ミネラル機能水の製造方法</td>
                      <td data-label="発明者">古崎　孝一</td>
                      <td data-label="特許権者">株式会社理研テクノシステム 株式会社Santa Mineral</td>
                      <td data-label="ステータス"><a href="" target="_blank" class="link-icon pdf">申請済</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>