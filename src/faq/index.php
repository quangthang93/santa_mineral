<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-company.php'; ?>
<main class="main p-end">

  <div class="p-end--cnt">
    <div class="container">

      <div class="breadcrumbWrap">
        <div class="breadcrumb">
          <ul>
            <li><a href="/">トップページ</a></li>
            <li>よくあるご質問</li>
          </ul>
        </div>
      </div><!-- ./breadcrumbWrap -->

      <section class="p-end--banner type2">
        <h1 class="p-end--ttl">
          <img src="<?php echo $PATH;?>/assets/images/end/ttl-faq.png" alt="">
          <span>よくあるご質問</span>
        </h1>
      </section><!-- ./p-recruit--banner -->

      <div class="p-faq">
        <div class="p-company--infor">
          <ul class="anchor--list">
            <li>
              <a href="#subject01" class="link-anchor">QQテクノロジー</a>
            </li>
            <li>
              <a href="#subject02" class="link-anchor">QQウォーターシリーズ</a>
            </li>
            <li>
              <a href="#subject03" class="link-anchor">QQセラミック</a>
            </li>
            <li>
              <a href="#subject04" class="link-anchor">QQ栽培</a>
            </li>
            <li>
              <a href="#subject05" class="link-anchor">QQ環境浄化</a>
            </li>
            <li>
              <a href="#subject06" class="link-anchor">ご注文</a>
            </li>
            <li>
              <a href="#subject07" class="link-anchor">講演依頼</a>
            </li>
            <li>
              <a href="#subject08" class="link-anchor">その他</a>
            </li>
          </ul><!-- .p-company--infor-bar -->
          <div class="p-company--infor-cnt">
            <div class="p-faq--row" id="subject01">
              <h2 class="title-lv2">QQテクノロジー</h2>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">QQテクノロジーとは、どんな技術ですか？</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>本文パターン2：lineheight1.6＞私たちは、テラヘルツ機能材料及びその応用製品の開発・製造・ 技術提供を行う企業です。当社の独自技術「QQテクノロジー」は、一切の化学物質を使用せずに、対象物に対し有効な働きをもたらします。この技術は、環境改善などで実証されており、医療や生活環境、農業、工業など、あらゆる産業に応用が可能です。</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
            </div><!-- .p-faq--row -->   
            <div class="p-faq--row" id="subject02">
              <h2 class="title-lv2">QQウォーターシリーズ</h2>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
            </div><!-- .p-faq--row -->  
            <div class="p-faq--row" id="subject08">
              <h2 class="title-lv2">その他</h2>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-label js-accorLabel">利用中ですが、クラウド版とパッケージ版の見分け方がわかりません。</div>
                <div class="accordion-cnt js-accorCnt">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
            </div><!-- .p-faq--row -->   
          </div><!-- .p-company--infor-cnt -->
        </div><!-- .p-company--infor -->
      </div><!-- ./p-faq -->
      
    </div>
  </div>

  <div class="align-center mgt-60">
    <a href="/" class="viewmore2">トップページへ戻る</a>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>