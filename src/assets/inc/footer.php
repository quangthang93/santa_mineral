		<footer class="footer js-footer">
      <a href="javascript:void(0)" class="backtop js-backtop">
        <div class="backtop-inner"><span>PAGE TOP</span></div>
      </a>
      <div class="footer-cnt">
        <div class="footer-head--infor">
          <div class="footer-head--contact">
            <p class="footer-head--contact-ttl">CONTACT</p>
            <p class="desc">QQテクノロジーの導入のご相談はお気軽にお問い合わせください。</p>
            <a href="/contact" class="link-pink"><span>お問い合わせ・ご注文</span></a>
          </div>
          <div class="footer-head--access">
            <div class="footer-head--logo">
              <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo-header.png" alt=""></a>
            </div>
            <div class="footer-head--addressWrap">
              <p class="footer-head--address">〒105-0013 東京都港区浜松町二丁目6番4号ガリシア浜松町1401号</p>
              <a class="footer-head--direct viewmore" href="" target="_blank">Google Map</a>
            </div>
          </div>
        </div>
        <ul class="footer-head--nav">
          <li><a class="link" href="">当社について</a></li>
          <li><a class="link" href="">QQ TECHNOLOGYとは</a></li>
          <li><a class="link" href="">技術・製品情報</a></li>
          <li><a class="link" href="">よくあるご質問</a></li>
          <li><a class="link" href="">お問い合わせ</a></li>
          <li><a class="link" href="">ニュース・レポート</a></li>
          <li><a class="link" href="">MOVIE</a></li>
          <li><a class="link" href="">会社情報</a></li>
          <li><a class="link" href="">保有特許</a></li>
          <li><a class="link" href="">プライバシポリシー</a></li>
        </ul>
      </div>
      <div class="footer-copy">
        <p>Copyright © 2020 Santa Mineral All rights reserved.</p>
      </div>
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/top.js"></script> -->
</body>

</html>